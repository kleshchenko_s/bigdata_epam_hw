-- create database `test_shema`
CREATE DATABASE test_schema;

-- create database `test_shema2` with explicit location
CREATE DATABASE test_schema2
LOCATION /warehouse/tablespace/external/hive/test_schema.db;

-- list all databases
SHOW DATABASES;

-- list all databases which name has '2' in its name
SHOW DATABASES '*2*';

-- drop database `test_shema2`
DROP DATABASE test_schema2;

-- (everything below do in scope of `test_shema` database)
USE test_schema;

-- create managed table `tab1e_1` with columns (`id` - type integer, `name` - type string)
CREATE TABLE IF NOT EXISTS table_1
(id INT, name STRING);

-- insert (1, 'aaa'), (2, 'bbb') into table `tab1e_1`
INSERT INTO TABlE table_1 VALUES (1,'aaa'), (2,'bbb');

-- show content of the table `tab1e_1`
SELECT * FROM table_1;

-- add column (`type` - type int) into table `tab1e_1` after column `id` and before column `name`
ALTER TABLE table_1 ADD COLUMNS (type INT);
ALTER TABLE table_1 CHANGE type type INT AFTER id; --??? doesn't work
CREATE TABLE table_1_1 AS SELECT id, type, name FROM table_1;
DROP TABLE table_1;
ALTER TABLE table_1_1 RENAME TO table_1;

-- show content of the table `tab1e_1`
SELECT * FROM table_1;

-- append table `tab1e_1` with (3, 2, 'ccc')
INSERT INTO TABLE table_1 VALUES (3, 2, 'ccc');

-- show content of the table `tab1e_1`
SELECT * FROM table_1;

-- convert this table `tab1e_1` into bucket table with bucket column `type`
CREATE TABLE table_1_bucketed (id INT, type INT, name STRING) CLUSTERED BY (type) INTO 6 BUCKETS;
INSERT OVERWRITE TABLE table_1_bucketed SELECT id, type, name FROM table_1;
DROP TABLE table_1;

-- show content of the table `tab1e_1`
SELECT * FROM table_1_bucketed;

-- via hdfs show the list of all files which are used by `tab1e_1`
SELECT DISTINCT FILE FROM (SELECT *, INPUT__FILE__NAME AS FILE FROM table_1_bucketed) t;

-- [?in several steps?] make table `tab1e_1` consists of 1 file (in hdfs)
CREATE TABLE table_1_1 AS SELECT id, type, name FROM table_1_bucketed;
SELECT DISTINCT FILE FROM (SELECT *, INPUT__FILE__NAME AS FILE FROM table_1) t;
DROP TABLE table_1;
ALTER TABLE table_1_1 RENAME TO table_1;
DROP TABLE IF EXISTS table_1_bucketed;

-- change table `table_1` into external
ALTER TABLE table_1 SET TBLPROPERTIES('EXTERNAL'='TRUE');

-- show table schema of `tab1e_1` to be sure table is external
DESC FORMATTED table_1;

-- drop table `tab1e_1`
DROP TABLE IF EXISTS table_1;

-- create new unmanaged table `tab1e_2` (w/o bucketing) with the content of the previous table `tab1e_1`
CREATE EXTERNAL TABLE IF NOT EXISTS table_2 (id INT, type INT, name STRING);
LOAD DATA INPATH '/warehouse/tablespace/managed/hive/test_schema.db/table_1' INTO TABLE table_2;

-- show content of the table `tab1e_2`
SELECT * FROM table_2;

-- [?in several steps?] change this table into unmanaged partitioned by `type` column
CREATE TABLE IF NOT EXISTS table_2a AS SELECT * FROM table_2;
DROP TABLE table_2;
CREATE TABLE table_2 (id INT, name STRING) PARTITIONED BY (type INT);
INSERT OVERWRITE TABLE table_2 PARTITION (type) SELECT * FROM table_2a;
DROP TABLE table_2a;

-- add new column (`action_time` - type date) at the latest column
ALTER TABLE table_2 ADD COLUMNS (action_time DATE);
CREATE TABLE IF NOT EXISTS table_2_temp AS SELECT * FROM table_2;
DROP TABLE table_2;
CREATE TABLE table_2 AS SELECT id, name, action_time, type FROM table_2_temp;

-- show content of the table `tab1e_2`
SELECT * FROM table_2;

-- append table `tab1e_2` with (4, 'ddd', '2018-10-31', 2), (5, 'eee', '2019-11-01', 3)
-- show content of the table `tab1e_2`
INSERT INTO TABLE table_2 VALUES (4, 'ddd', '2018-10-31', 2), (5, 'eee', '2019-11-01', 3);

-- show data ordered by `action_time`
SELECT * FROM table_2 ORDER BY action_time;

-- show data which `action_time` is greater than today (today is not hardcoded)
SELECT * FROM table_2 WHERE action_time > current_date();

-- show data which `action_time` is in 2019 year
SELECT * FROM table_2 WHERE action_time > '2018-12-31' and action_time < '2020-01-01';

-- for every `type` show first record only (orderied by `name` column)
SELECT * FROM (SELECT type, MIN(name) FROM test_schema.table_2 GROUP BY type) t;

-- do previous step using OVER clause
-- ?

-- list all partitions of `tab1e_2`
SHOW PARTITIONS test_schema.table_2;

-- drop all partitions except partition '2' ('type=2')
ALTER TABLE table_2 DROP IF EXISTS PARTITION (type != '2');

-- show content of the table `tab1e_2`
SELECT * FROM table_2;

-- create new managed table `tab1e_3` from the select of `table_2`
CREATE TABLE table_3 AS
SELECT * FROM table_2
--        with specific location
LOCATION '/user/databases'
--        with compression snappy
--        with type orc
STORED AS ORC TBLPROPERTIES ('orc.compress' = 'SNAPPY');

-- show content of table `tab1e_3`
SELECT * FROM table_3;

-- list all tables in database `test_shema`
USE test_schema;
SHOW TABLES;

-- list all tables in database `test_shema` which name contains '3'
SHOW TABLES LIKE '*3*';

-- drop test_shema (in 1 command)
DROP DATABASE test_schema;