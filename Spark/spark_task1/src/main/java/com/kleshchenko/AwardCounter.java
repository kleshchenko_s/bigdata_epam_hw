package com.kleshchenko;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

public final class AwardCounter {
    public static void main(String[] args) throws Exception {
        SparkSession spark = SparkSession
                .builder()
                .master("local")
                .appName("Awards")
                .getOrCreate();

        JavaRDD<String> playerRDD = spark.read().textFile("Master.csv")
                .javaRDD();
        JavaRDD<String> awardsRDD = spark.read().textFile("AwardsPlayers.csv")
                .javaRDD().map(s -> s.split(",")[0]);
        JavaPairRDD<String, Integer> counts  = awardsRDD.mapToPair(s -> new Tuple2<>(s, 1)).reduceByKey((i, j) -> i + j);
        JavaPairRDD<String, String> idAndFullName = playerRDD.mapToPair(s -> {
            String id = s.split(",")[0];
            String fullName = s.split(",")[3] + " " + s.split(",")[4];
            return new Tuple2<>(id, fullName);
        });
        JavaPairRDD<String, Tuple2<String,Integer>> fullRDD = idAndFullName.join(counts);

        JavaRDD<String> result = fullRDD.map((s) -> s._2._1 + " - " + s._2()._2);
        result.collect().forEach(s -> System.out.println(s));
        result.coalesce(1).saveAsTextFile("result.csv");
        spark.stop();
    }
}